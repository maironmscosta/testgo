package notify

import (
	"bitbucket.org/bemobidev/log"
	"bitbucket.org/bemobidev/tracker-api/model"
	"bitbucket.org/bemobidev/tracker-api/repository"
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
)

type Notify struct {
	Logger      *log.Context
	TrackerDB   repository.TrackerDB
	HttpClients map[string]*http.Client
}

const TransactionSuccess = "success on process notify"
const TransactionError = "error on process notify"

func (n *Notify) MustInit(logger *log.Context, db repository.TrackerDB) {
	n.Logger = logger
	n.TrackerDB = db
	templates, err := n.TrackerDB.GetPartnerTemplates()
	if err != nil {
		n.Logger.F("error on list request templates", "err", err)
		return
	}

	n.HttpClients = make(map[string]*http.Client)
	for _, template := range templates {
		var requestConfiguration model.RequestConfiguration
		if err := json.Unmarshal(template.Request, &requestConfiguration); err != nil {
			n.Logger.F("error on unmarshal template request on custom client config",
				"err", err,
				"requestSTR", template.Request)
		}

		maxIdleConnections, err := strconv.Atoi(requestConfiguration.Client["max_idle_connections"])
		if err != nil {
			n.Logger.F("error on parser max_idle_connections to int", "err", err, "template", template)
			return
		}
		requestTimeOut, err := strconv.Atoi(requestConfiguration.Client["request_timeout"])
		if err != nil {
			n.Logger.F("error on parser request_timeout to int", "err", err, "template", template)
			return
		}
		n.HttpClients[template.Name] = createHTTPClient(maxIdleConnections, requestTimeOut)
		n.Logger.I("create new client with success",
			"template_name", template.Name,
			"max_idle_connections", maxIdleConnections,
			"request_time_out", requestTimeOut)
	}
}

func createHTTPClient(maxIdleConnections, requestTimeout int) *http.Client {
	return &http.Client{
		Transport: &http.Transport{
			MaxIdleConnsPerHost: maxIdleConnections,
		},
		Timeout: time.Duration(requestTimeout) * time.Second,
	}
}

func (n *Notify) CallTracker(tracker model.Tracker, event model.NotifyRequest) (*model.NotifyResponse, error) {
	start := time.Now()

	var response model.NotifyResponse
	template, err := n.TrackerDB.GetPartnerTemplateByID(tracker.PartnerTemplateID)
	if err != nil {
		response.Status = TransactionError
		response.Details = fmt.Sprintf("error on retrieve notify template in db details [%s]", err.Error())
		return nil, err
	}

	if template == nil {
		response.Status = TransactionError
		response.Details = fmt.Sprintf("notify template not found in db details [%s]", err.Error())
		return nil, err
	}

	integrationUrl, err := n.replaceTokenFromUrl(tracker, template, event)
	if err != nil {
		response.Status = TransactionError
		response.Details = fmt.Sprintf("error on build tracker integration url details [%s]", err.Error())
		return &response, err
	}
	response.Url = integrationUrl

	partnerResponse, err := n.executeTracker(integrationUrl, template)
	if err != nil {
		n.Logger.E("error on execute tracker integration", "url", integrationUrl,"err", err)
		response.Status = TransactionError
		response.Details = fmt.Sprintf("error on execute tracker integration url details [%s]", err.Error())
		return nil, err
	}
	response.ResponseCode = partnerResponse.ResponseStatusCode
	response.Response = partnerResponse.ResponseBody
	response.Status = TransactionSuccess
	response.ResponseElapsedTime = time.Since(start).String()

	n.Logger.I("tracker integration executed successfully",
		"tracker_id", tracker.ID,
		"msisdn", event.Msisdn,
		"tracker_name", tracker.TrackerName,
		"service_id", int(event.SubscriptionServiceId),
		"service_name", event.SubscriptionServiceName,
		"channel", tracker.Channel,
		"campaign", tracker.Campaign,
		"command", tracker.Command,
		"url", tracker.TrackingUrl,
		"subscription_id", event.SubscriptionID,
		"carrier", event.Carrier,
		"partner_response", response.ResponseCode,
		"partner_response_time", response.ResponseElapsedTime,
		"partner_response_status", response.Status,
		"partner_response_detail", response.Details,
		"partner_response_url", response.Url,
		"partner_response_body", partnerResponse.ResponseBody)

	response.ResponseElapsedTime = time.Since(start).String()
	return &response, nil
}

func (n *Notify) replaceTokenFromUrl(tracker model.Tracker, template *model.PartnerTemplate, event model.NotifyRequest) (string, error) {

	integrationUrl := tracker.TrackingUrl
	tokens := strings.Split(template.Tokens, ",")

	for _, token := range tokens {
		switch token {
		case "%msisdn%":
			integrationUrl = strings.Replace(integrationUrl, "%msisdn%", event.Msisdn, 1)
			integrationUrl = strings.Replace(integrationUrl, "%MSISDN%", event.Msisdn, 1)

		case "%affid%":
			subscriptionID := strconv.FormatInt(event.SubscriptionID, 10)
			affiliate, err := n.TrackerDB.GetAffiliateBySubscriptionID(subscriptionID)
			if err != nil {
				return "", errors.Wrapf(err, "error on retrieve affiliate by subscription ID [%s] in db", subscriptionID)
			}
			if affiliate == nil {
				return "", errors.New(fmt.Sprintf("affiliate by subscription ID [%s] not found in db", subscriptionID))
			}
			integrationUrl = strings.Replace(integrationUrl, "%affid%", affiliate.AffiliateSubscriptionID, 1)
		}
	}
	return integrationUrl, nil
}

func (n *Notify) executeTracker(url string, template *model.PartnerTemplate) (*model.Response, error) {

	var requestConfig model.RequestConfiguration
	var response model.Response

	if err := json.Unmarshal(template.Request, &requestConfig); err != nil {
		n.Logger.E("error on unmarshal request template", "err", err)
		return nil, err
	}

	method := requestConfig.Method
	client := n.HttpClients[strings.ToLower(template.Name)]

	req, err := http.NewRequest(method, url, nil)
	if err != nil {
		n.Logger.E("error on build request",
			"url", url,
			"method", method,
			"err", err)
		return nil, err
	}

	for key, value := range requestConfig.Headers {
		req.Header.Add(key, value)
	}

	res, err := client.Do(req)
	if err != nil {
		n.Logger.E("error on execute tracker by template configuration",
			"url", url,
			"template", template,
			"err", err)
		return nil, err
	}

	defer res.Body.Close()

	bodyBytes, err := ioutil.ReadAll(res.Body)
	if err != nil {
		n.Logger.E("error on read notify response",
			err, "err",
			"url", url)
		response.ResponseBody = fmt.Sprintf("error on ready response err:{%s}", err.Error())
		response.ResponseStatusCode = res.StatusCode
		return &response, nil
	}
	response.ResponseBody = string(bodyBytes)
	response.ResponseStatusCode = res.StatusCode

	return &response, nil
}
