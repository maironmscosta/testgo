package settings


var Settings struct {

	Server struct {
		Port    string `envconfig:"default=5000"`
		Context string `envconfig:"default=tracker-api"`
	}

	Database struct {
		UserName        string `envconfig:"default=subs_notifier"`
		Password        string `envconfig:"default=2G9WExbN"`
		Host            string `envconfig:"default=subscription-notifier.cpc8rojhpfzl.us-east-1.rds.amazonaws.com"`
		Port            string `envconfig:"default=5432"`
		DatabaseName    string `envconfig:"default=subscription_notifier"`
		SetMaxOpenConns int    `envconfig:"default=100"`
		LogMode         bool   `envconfig:"default=false"`
	}

	Redis struct {
		BaseURL string        `envconfig:"default=redis://127.0.0.1:6379?db=0"`
	}

	Tracker struct {
		PaginationMaxPerPage int `envconfig:"default=10"`
	}

	AffiliateTracker struct {
		PaginationMaxPerPage int `envconfig:"default=10"`
	}

	PartnerTemplate struct {
		PaginationMaxPerPage int `envconfig:"default=10"`
	}

	SQS struct {
		BufferSize  int `envconfig:"default=1000"`
		Connections []struct {
			QueueName    string `envconfig:"default=tracker-notifications-dev"`
			ConnectionURL string `envconfig:"default=sqs://:@us-east-1?ReceiveMessageWaitTimeSeconds=20"`
		} `envconfig:"optional"`
	}
}