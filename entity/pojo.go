package entity

import (
	"encoding/json"
	"fmt"
	"time"
)

type Post struct {
	Id         string    `json:"id"`
	Title      string    `json:"title"`
	ActiveDays int       `json:"activeDays"`
	Code       string    `json:"code"`
	Comments   []Comment `json:"comments"`
	Date       Date      `json:"date"`
	// ExpiredDate *commentDate `json:"expiredDate"`
}

// type Comment struct {
// 	Description string    `json:"description"`
// 	Date        time.Time `json:"date"`
// }
type Comment struct {
	Description string `json:"description"`
	Date        Date   `json:"date"`
}

type Date struct {
	Year       int `json:"year"`
	Month      int `json:"month"`
	DayOfMonth int `json:"dayOfMonth"`
	HourOfDay  int `json:"hourOfDay"`
	Minute     int `json:"minute"`
	Second     int `json:"second"`
}

func (d *Date) FormatStructDate() string {
	return fmt.Sprintf("%v/%v/%v", d.Year, (d.Month + 1), d.DayOfMonth)
}

type commentDate struct {
	Time time.Time `json:"-"`
	Date struct {
		Year       int `json:"year"`
		Month      int `json:"month"`
		DayOfMonth int `json:"dayOfMonth"`
		HourOfDay  int `json:"hourOfDay"`
		Minute     int `json:"minute"`
		Second     int `json:"second"`
	} `json:"date"`
}

func (c *commentDate) UnmarshalJSON(d []byte) error {
	type Alias commentDate
	aux := &struct {
		*Alias
	}{
		Alias: (*Alias)(c),
	}

	if err := json.Unmarshal(d, &aux); err != nil {
		return err
	}
	date := aux.Date
	c.Time = time.Date(date.Year, time.Month(date.Month), date.DayOfMonth, date.HourOfDay, date.Minute, 0, 0, time.Local)

	return nil
}
