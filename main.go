package main

import (
	"fmt"
	"net/http"

	"testgo/handle"
)

func main() {

	ara := handle.AraHandler{
		URLARAService: "https://ara-service.herokuapp.com",
		URLSQS:        "sqs://:@us-east-1?ReceiveMessageWaitTimeSeconds=20",
		SQSQueueName:  "control-plane-dev",
	}

	http.HandleFunc("/home", ara.GetHomeList)
	fmt.Println("Server is up and listening on port 9090.")
	http.ListenAndServe(":9090", nil)
}
