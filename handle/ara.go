package handle

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"testgo/entity"
)

type AraHandler struct {
	// UrlARAService string `envconfig:"default=https://ara-service.herokuapp.com"`
	URLARAService string
	URLSQS        string
	SQSQueueName  string
}

func (ara *AraHandler) GetHomeList(w http.ResponseWriter, r *http.Request) {

	// var URL_ARA_SERVICE = "https://ara-service.herokuapp.com"

	// search := URL_ARA_SERVICE + "/post/v1/search-post/1"

	search := ara.URLARAService + "/post/v1/detail/2019USQM"

	fmt.Println("search: ...", search)

	resp, err := http.Get(search)
	if err != nil {
		fmt.Println("err...", err)
	}

	// fmt.Println("resp...", resp)

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	// fmt.Println("body...", body)
	jsonBody := string(body)

	fmt.Println("jsonBody...", jsonBody)

	post := entity.Post{}
	err = json.Unmarshal(body, &post)
	if err != nil {
		fmt.Println("error", err)
	}

	fmt.Printf("post... %#+v %v %v \n", post.Id, " - ", post.Date.FormatStructDate())

	// postList := []entity.Post{}
	// json.Unmarshal(body, &postList)

	// fmt.Println("postList...", postList)
	tpl, _ := template.ParseFiles("page/test.html")
	data := map[string]string{
		"Json": fmt.Sprintf("%#+v %v %v", post.Id, " - ", post.Date.FormatStructDate()),
	}
	w.WriteHeader(http.StatusOK)
	tpl.Execute(w, data)

}
