package consumer

import (
	"bitbucket.org/bemobidev/queue"
	"bitbucket.org/bemobidev/cache"
	"bitbucket.org/bemobidev/log"
	"github.com/pkg/errors"
	"bitbucket.org/bemobidev/tracker-api/model"
	"encoding/json"
	"bitbucket.org/bemobidev/tracker-api/repository"
	"bitbucket.org/bemobidev/tracker-api/notify"
	"strconv"
	"strings"
)

// New configures a new consumer
func New(broker queue.DataBroker, cache cache.Interface, db repository.TrackerDB,
	logger *log.Context, notify notify.Notify) (*Consumer, error) {
	return &Consumer{
		Broker:            broker,
		Cache:             cache,
		TrackerDB:         db,
		Logger:            logger,
		IntegrationNotify: notify,
	}, nil
}

// Consumer is a SQS consumer
type Consumer struct {
	Broker            queue.DataBroker
	Cache             cache.Interface
	TrackerDB         repository.TrackerDB
	Logger            *log.Context
	IntegrationNotify notify.Notify
}

// Loop starts a listener in background or returns an error
func (c *Consumer) Loop(queueName string) error {
	events, err := c.Broker.Listen(queueName, queue.Forever)
	if err != nil {
		return errors.Wrap(err, "could not start event listener")
	}

	go func() {
		for event := range events {
			msg, err := event.Bytes()
			if err != nil {
				c.Logger.E("unable to parse event ", "err", err)
				event.Nack(false)
				continue
			}
			if err := c.consume(msg); err != nil {
				c.Logger.E("failed to consume message", "err", err)
				event.Nack(false)
				continue
			}
			event.Ack()
		}
	}()

	return nil
}

func (c *Consumer) consume(msg []byte) error {
	var notification model.NotifyConsumer
	if err := json.Unmarshal(msg, &notification); err != nil {
		return errors.Wrap(err, "unable to unmarshal event")
	}

	logger := c.Logger.C("msisdn", notification.Msisdn,
		"service_id", notification.SubscriptionServiceId,
		"service_name", notification.SubscriptionServiceName,
		"channel", notification.Channel,
		"command", notification.SmsCommand,
		"campaign", notification.Campaign,
		"carrier", notification.Carrier)

	trackers, err := c.TrackerDB.GetTrackersByFilters(notification.SubscriptionServiceId,
		strings.TrimSpace(notification.Carrier), strings.TrimSpace(notification.Channel),
		strings.TrimSpace(notification.SmsCommand), strings.TrimSpace(notification.Campaign))

	if err != nil {
		return errors.Wrap(err, "error on retrieve tracker by filters in db")
	}

	if len(trackers) == 0 {
		logger.W("partner tracker not found or not active by filters")
	}

	for _, tracker := range trackers {
		logger.I("call notify tracker by subscription event [START]")

		value , err := c.Cache.Get(strconv.Itoa(tracker.ID))
		if err != nil {
			logger.W("error to get value from redis")
		}

		count := 0
		if value != "" {
			count, err = strconv.Atoi(value)
			if err != nil {
				return errors.Wrap(err, "error parsing redis value from string to int")
			}
		}

		if count >= tracker.MaxHitDay {
			logger.W("did not call notify because of the max hit day")
			continue
		}

		notificationRequest := model.NotifyRequest(notification)
		_, err = c.IntegrationNotify.CallTracker(tracker, notificationRequest)
		if err != nil {
			return errors.Wrap(err, "call notify tracker by subscription event")
		}

		logger.I("call notify tracker by subscription event")

		_, err = c.Cache.Incr(strconv.Itoa(tracker.ID))
		if err != nil {
			return errors.Wrap(err, "error doing incr in redis")
		}

	}
	return nil
}